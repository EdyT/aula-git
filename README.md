# Instalando e configurando

Faça o [download do Git](https://git-scm.com/) ou instale-o a partir de um package manager (ex.: `apt install git`)

Após instalar, é necessário configurar o git (Não precisa criar conta nenhuma!)  
Abra o terminal (ou Git Bash, no Windows) e configure com suas informações de contato:

```bash
git config --global user.name "Seu nome"
git config --global user.email "seu-email@example.com"
git config color.ui true #(opcional)
```
Seu Git já está pronto para ser usado!

# Cheat Sheet
Referência para os comandos mais utilizados

![GitTower CheatSheet image](images/git-cheat-sheet-large01.png)  
_[link para página completa](https://www.git-tower.com/blog/git-cheat-sheet/)_

# Lifecycle
![](images/git_file_lifecycle.jpeg)  
[_Imagem de Daniel Krummer_](https://www.slideshare.net/origamiaddict/git-get-ready-to-use-it)


> **Untracked**: Novo arquivo! Não será incluído no commit a menos que adicionado explicitamente  
> **Staged**: Arquivos marcados para serem incluídos no próximo commit  
> **Unmodified**: Arquivos sem modificações em relação ao último commit  
> **Modified**: Arquivos modificados em relação ao último commit  

# Workflow típico

1. Crie uma nova branch para a feature (funcionalidade) que será implementada
2. Modifique/Crie os arquivos necessários
3. `git add` neles!
4. Revise e teste a feature
5. Crie um commit com uma mensagem apropriada
6. Suba suas alterações para o repositório remoto com `git push`
7. Repita

# Trabalhando com repositórios remotos
Os comandos típicos para interagir com repositórios remotos são:
* `git push` - Atualizar o repositório remoto com os seus commits
* `git pull` - Atualizar o seu repositório local com os novos commits do repositório remoto
* `git fetch` - Visualizar atualizações novas do repositório remoto
* `git clone` - Clonar um repositório


# Comandos perigosos
Liberdade de mais às vezes é ruim! Cuidado com alguns comandos!  
* Muita atenção quando for desfazer algum commit, ou editar
* Verifique se você não alterou a ordem dos commits antes de enviar para o `origin` (repositório remoto principal)



# Boas práticas
Ao trabalhar em grupo, tente seguir boas práticas de commits!  
(_Sério... isso ajuda demais_)  
**https://chris.beams.io/posts/git-commit/#seven-rules**

# Alterando mensagens dos commit (aviso)