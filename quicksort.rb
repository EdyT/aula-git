def partition(array, l, h)
  pivot = array[h]
  
  i = l

  for j in l..h
    if array[j] > pivot
      array[i], array[j] = array[j], array[i]
      i += 1
    end
  end
  
  array[i], array[h] = array[h], array[i]

  i
end

def quicksort_q(array, l, h)
  if (l < h)
    p = partition(array, l, h)
    quicksort_q(array, l, (p-1))
    quicksort_q(array, (p+1), h)
  end
end

def quicksort(array)
  quicksort_q(array, 0, array.size-1)
end

def generate_array(n = 10)
  array = []
  n.times { array << rand(1..100) }
  array
end

def run()
  array = generate_array()
  
  puts "array: #{array.inspect} \n\n"
  quicksort(array)
  puts "result: #{array.inspect} \n\n"
end
